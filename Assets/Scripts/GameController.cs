﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameObject cube;
    private int xIndex = 10;
    private int yIndex = 5;
    private int zIndex = 10;
    void Start()
    {
        CreateCubes();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("GameDemo");
    }

    private void CreateCubes()
    {
        for (int z = 0; z < zIndex; z++)
        {
            for (int x = 0; x < xIndex; x++)
            {
                for (int y = 0; y < yIndex; y++)
                {
                    Instantiate(cube, new Vector3(x, y, z), Quaternion.identity);
                }
            }
        }
    }
}
