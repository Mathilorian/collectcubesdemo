﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshCollider))]
public class Joystick : MonoBehaviour
{
    Vector3 startTouch;
    Vector3 endTouch;
    Vector3 direction;

    // Update is called once per frame
  
    public float horizontalSpeed = 2.0F;
    public float verticalSpeed = 2.0F;

    private Vector3 targetPos;
    private Vector3 startPos;
    public LayerMask hitLayers;

    Vector3 mouse;


    private float vertical;
    private float horizontal;

    private float yPos;
   
    private void Start()
    {
        yPos = transform.position.y;
    }

    void FixedUpdate()
    {
        //ControlPlayer();
        ControlPlayerWithMouse();
    }

    private void ControlPlayer()
    {
        mouse = Input.mousePosition;
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                RaycastHit hit;
                if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, hitLayers))
                {
                    startPos = hit.point;
                }
            }

            Vector3 movePos = targetPos - startPos;
            transform.LookAt(new Vector3(movePos.x, transform.position.y, movePos.z));
            transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
            if (touch.phase == TouchPhase.Moved)
            {
                Ray castPoint = Camera.main.ScreenPointToRay(mouse);
                RaycastHit hit;
                if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, hitLayers))
                {
                    targetPos = hit.point;
                    movePos = targetPos - startPos;
                    gameObject.GetComponent<Rigidbody>().velocity = transform.forward * 30;
                }
            }
            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Ended)
            {
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }

    private void ControlPlayerWithMouse()
    {
        mouse = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, hitLayers))
            {
                startPos = hit.point;
            }
        }

        Vector3 movePos = targetPos - startPos;
        transform.LookAt(new Vector3(movePos.x, transform.position.y, movePos.z));
        transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
        if (Input.GetMouseButton(0))
        {
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, hitLayers))
            {
                targetPos = hit.point;
                movePos = targetPos - startPos;
                gameObject.GetComponent<Rigidbody>().velocity = transform.forward * 30;

            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}


