﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowAreaManager : MonoBehaviour
{
    [SerializeField]
    Material yellowMaterial;
    
    public void OnTriggerEnter(Collider other)
    {
        other.gameObject.layer = LayerMask.NameToLayer("YellowArea");
        other.gameObject.GetComponent<MeshRenderer>().material = yellowMaterial;
    }
}
